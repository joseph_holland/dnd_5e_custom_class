function loadTemplate() {
    vars = JSON.parse(sessionStorage.getItem("vars"));

    $(".template-title").text(vars.name);
    $(".template-name").text(vars.name);
    $(".template-description").text(vars.description);
    $(".template-hd").text(vars.hd);
    $(".template-bab").text(vars.bab);
    $(".template-saves").text(vars.saves);
    $(".template-skillpoints").text(vars.skillpoints);

    if(vars.alignrestrict.toLowerCase() != "none" || vars.equiprestrict.toLowerCase() != "none") {
        writeSpecial();
    }

    if (vars.skills == "none") /* skills */ {
        $(".template-skills").text(
            `${getIndefiniteArticle(vars.name, true)} ${vars.name} has no skills`
        );
    } else {
        $(".template-skills").text(
            `${getIndefiniteArticle(vars.name, true)} ${vars.name}'s class skills (and the key ability for each skill) are ${vars.skills}`
        );
    }

    $(".template-equipmentprof").text(
        `${getIndefiniteArticle(vars.name, true)} ${vars.name} is proficient with ${vars.weaponprof}, 
        ${vars.armourprof}, and ${vars.shieldprof}`
    );

    buildTable();
    writeDescriptions();
    buildCastingTables();
}

function writeSpecial() {
    special = `
    <h4><i>Special</i></h4><br>`;

    let added_space = false;
    if (vars.alignrestrict.toLowerCase() != "none") {
        special += `
        <div class="text-danger"><b>Alignment Restriction</b></div>
        <div>${vars.alignrestrict}</div><br>
        `;
        added_space = true;
    }
    if (vars.equiprestrict.toLowerCase() != "none") {
        
        special += `
        <div class="text-danger"><b>Equipment Restriction</b></div>
        <div>${vars.equiprestrict}</div><br>
        `
        added_space = true;
    }
    if (!added_space) special += "<br>";
    $(".template-special").html(special);
}

function getValue(stat, save="") {
    switch(stat) {
        case "bab":
            switch(vars.bab) {
                case "1/2":
                    return 0.5;
                case "3/4":
                    return 0.75;
                case "1/1":
                    return 1;
            }
            break;
        case "save":
            return  vars.saves.toUpperCase().includes(save.toUpperCase());
        default: 
            return false;
    }
}

function buildTable() {
    var string = "";

    var bab = getValue("bab");
    var fort = getValue("save", "fortitude");
    var ref = getValue("save", "reflex");
    var will = getValue("save", "will");

    for (var i = 0; i < 20; i++) {
        level = i+1
        string += 
        `
        <tr>
        <th scope="row">${i+1}</th>
        <td>${Math.floor(bab*level)}</td>
        <td>${Math.floor(fort ? 2 +level/2 : level/3)}</td>
        <td>${Math.floor(ref ? 2 +level/2 : level/3)}</td>
        <td>${Math.floor(will ? 2 +level/2 : level/3)}</td>
        <td>${vars.special[i].join(", ")}</td>
        </tr>
        `
    }

    $(".template-table").html(string);
}

function getDescriptionHTML(title, description) {
    return `
    <div class="text-danger"><b>${title}</b></div>
    <div>${description}</div>
    <br>
    `;
}

function getCastStat(stat) {
    switch (stat.toLowerCase()) {
        case "str":
            return "Strength"
        case "dex":
            return "Dexterity"
        case "con":
            return "Constitution"
        case "int":
            return "Intelligence"
        case "wis":
            return "Wisdom"
        case "cha":
            return "Charisma"
    }
}

function getIndefiniteArticle(word, up=false) {
    let vowels = ["a", "e", "i", "o", "u"];
    let article = vowels.includes(word.charAt(0).toLowerCase()) ? "an" : "a";
    if (up) {article = article.charAt(0).toUpperCase() + article.slice(1)};
    return article;
}

function getFirstLevelKnownSpellAmounts(SPELLTYPE, CASTTYPE, CASTSTAT, PROGRESSION) {
    // return string
    // zero level, first level. Extra spells
    let divine = (SPELLTYPE.toLowerCase() == "divine");

    let string = ``;
    switch(PROGRESSION.toLowerCase()) {
        case "slow":
            case "spontaneous":
                string += `Spells are not gained until level 4.`;
                break;
            case "vancian":
                string += (divine) 
                ? `Spells are not gained until level 4. Once available, All spells are known from each available level.` 
                : `Spells are not gained until level 4.`;
                break;
            break;
        case "medium":
            switch(CASTTYPE.toLowerCase()) {
                case "spontaneous":
                    string += `Four 0-level spells.`;
                    break;
                case "vancian":
                    string += `All 0-level spells are known.`;
                    break;
            }
            break;
        case "full":
            switch(CASTTYPE.toLowerCase()) {
                case "spontaneous":
                    string += `Four 0-level spells and two 1st-level spells.`;
                    break;
                case "vancian":
                    string += (divine) ? `All 0-level and 1st-level spells are known.` : `All 0-level spells and three 1st-level spells.
                    Additionally, for each point of ${CASTSTAT} modifier, an extra 1st-level spell is known.`;
                    break;
            }
            break;
        case "known":
            string += `All spells are known.`
    }
    return string;
}

function getSpellProgression(FEATUREID, VALUE) {
    let progression = ``;
    switch(FEATUREID) {
        case "f-arcanecast":
            switch(VALUE) {
                case "1":
                    progression = "slow";
                    break;
                case "3":
                    progression = "medium";
                    break;
                case "10":
                    progression = "full";
                    break;
            }
            break;
        case "f-divinecast":
            switch(VALUE) {
                case "1":
                    progression = "slow";
                    break;
                case "3":
                    progression = "medium";
                    break;
                case "8":
                    progression = "full";
                    break;
            }
            break;
    }
    return progression;
}

function getCastingString(SPELLBOOK, CASTTYPE, CASTSTAT, SPELLTYPE, PROGRESSION) {

    // NAME casts SPELLTYPE spells which are drawn from the SPELLBOOK spell list
    // NAME must/can cast CASTTYPE ahead of time
    // br
    // Casting Stat: CASTSTAT score equal to 10 + spell level
    // Spell Save DC: 10 + spell level + NAME's CASTSTAT modifier
    // br
    // NAME starts with X 0-level NAME spells and X 1st-level NAME spells
    // For each point of CASTSTAT bonus the NAME has, an additional 1st level spell is known

    let introdescript = `
    ${getIndefiniteArticle(vars.name, true)} ${vars.name} casts ${SPELLTYPE} spells which are drawn from the
    ${SPELLBOOK} spell list.
    `;
    switch(CASTTYPE.toLowerCase()) {
        case "spontaneous":
            introdescript += `${getIndefiniteArticle(vars.name, true)} ${vars.name} can cast any spell 
            he knows without preparing it ahead of time. `;
            break;
        case "vancian":
        default:
            introdescript += `${getIndefiniteArticle(vars.name, true)} ${vars.name} must choose and
            prepare her spells ahead of time. `;
            break;
    }

    let spell_failure = (SPELLTYPE.toLowerCase() == "arcane") 
    ? "He will also suffer from arcane spell failure when wearing armour or equipping shields." 
    : "He will not suffer any spell failure from wearing armour or equipping shields.";
    introdescript += spell_failure;

    let spells_known = getFirstLevelKnownSpellAmounts(SPELLTYPE, CASTTYPE, CASTSTAT, PROGRESSION);

    let castdescript = `
    <b>Casting Stat:</b> To learn or cast a spell, ${getIndefiniteArticle(vars.name, true)} ${vars.name} needs
    ${getIndefiniteArticle(CASTSTAT)} ${CASTSTAT} score of at least 10 + the spell level.
    <br>
    <b>Spell Save DC:</b> 10 + spell level + ${vars.name}'s ${CASTSTAT} modifier.
    <br>
    <b>Spells Known at Start:</b> ${spells_known}
    `;


    return `
    ${introdescript}
    <br>
    <br>
    ${castdescript}
    `;
}

function sortDescriptions(featuresarray) {
    let featurestrings = "";

    // loop through 20 levels
    for (let i = 1; i < 20; i++) {
        let temp = new Array();
        
        // loop through each featuresarray
        // assuming that it's been created correctly
        for (let j = 0; j < featuresarray.length; j++) {
            if (featuresarray[j][2] == i) {
                temp.push([featuresarray[j][0],featuresarray[j][1]]);
            }
        }
        temp.sort((a,b) => a[0].localeCompare(b[0]));

        // loop through temp, append to string
        for (let j = 0; j < temp.length; j++) {
            featurestrings += getDescriptionHTML(temp[j][0], temp[j][1]);
        }
    }
    return featurestrings;

}

function writeDescriptions() {
    // create multidimensional array
    // each element will have [name, description, level]
    // sort array by level and alphabetically, in that order
    // loop through array, convert to big string

    let featuresarray = new Array();

    for (let i = 0; i < vars.features.length; i++) {
        console.log(vars.features[0]);
        switch(vars.features[i][0]) {
            case "f-sneakattack":

                var sneaks = "";
                switch(vars.features[i][1]) {
                    case "1":
                        sneaks = "four";
                        break;
                    case "2":
                        sneaks = "three";
                        break;
                    case "3":
                        sneaks = "two";
                        break;
                }

                featuresarray.push([
                    "Sneak Attacks",
                    `
                    If ${getIndefiniteArticle(vars.name)} ${vars.name} can catch an opponent when he is unable to defend himself effectively from her attack, she can strike a vital spot for extra damage.
                    <br>
                    <br>
                    The ${vars.name}’s attack deals extra damage any time her target would be denied a Dexterity bonus to AC (whether the target actually has a Dexterity bonus or not), or when the ${vars.name} flanks her target. This extra damage is 1d6 at 1st level, and it increases by 1d6 every ${sneaks} ${vars.name} levels thereafter. Should the ${vars.name} score a critical hit with a sneak attack, this extra damage is not multiplied. 
                    `,
                    1
                ]);
            break;
            case "f-arcanecast":
                featuresarray.push([
                    "Arcane Spells", 
                    getCastingString(
                        vars.features[i][2][0],
                        vars.features[i][2][1],
                        getCastStat(vars.features[i][2][2]),
                        "arcane",
                        getSpellProgression(vars.features[i][0], vars.features[i][1])
                    ),
                    1
                ]);
            break;
            case "f-divinecast":
                featuresarray.push([
                    "Divine Spells", 
                    getCastingString(
                        vars.features[i][2][0],
                        vars.features[i][2][1],
                        getCastStat(vars.features[i][2][2]),
                        "divine",
                        getSpellProgression(vars.features[i][0], vars.features[i][1])
                    ),
                    1
                ]);
            break;
            case "f-rage":
                featuresarray.push([
                    "Rage",
                    `${getIndefiniteArticle(vars.name, true)} ${vars.name} can fly into a rage once per encounter, for a 
                    certain number of times per day. This is a free action, but can only be done on the ${vars.name}'s turn. At the end of the rage, the ${vars.name} becomes fatigued (-2 str, -2 dex, cannot charge or run)
                    for the duration of the encounter. The ${vars.name} may end the rage prematurely. The ${vars.name} gains increased hitpoints
                    due to the Constitution bonus, equal to 2 points per level, which are lost at the end of the rage. This could kill or drop the
                    character if not careful.
                    <br>
                    <br>
                    <b>Rage Benefits:</b> +4 bonus to Strength and Constitution, +2 morale bonus to Will saves
                    <br>
                    <b>Rage Drawbacks:</b> -2 penalty to armour class, cannot use Charisma or Intelligence skills (except Intimidate), nor actions that require
                    patience or concentration (for example, open locks, casting spells, using magical devices)
                    <br>
                    <b>Rage Duration:</b> 3 + the character's (newly improved) Constitution modifier
                    `,
                    1
                ]);

                featuresarray.push([
                    "Indomitable Will",
                    `At level 14, ${getIndefiniteArticle(vars.name)} ${vars.name}'s rage becomes more powerful. He can resist enchantment
                    spells while under rage.
                    <br>
                    <b>New Rage Benefit:</b> +4 bonus on Will saves vs enchantment spells (stacks with other bonuses)
                    `,
                    14
                ]);

                featuresarray.push([
                    "Tireless Rage",
                    `At level 17, ${getIndefiniteArticle(vars.name)} ${vars.name} no longer becomes fatigued at the end of his rage.
                    `,
                    17
                ]);
                
                featuresarray.push([
                    "Mighty Rage",
                    `At level 20, ${getIndefiniteArticle(vars.name)} ${vars.name}'s rage becomes more powerful. Benefits are enhanced
                    but drawbacks and duration remain the same.
                    <br>
                    <b>New Rage Benefits:</b> +8 bonus to Strength and Constitution, +4 morale bonus to Will saves
                    `,
                    20
                ]);
            break;
            case "f-damagereduction":
                featuresarray.push([
                    "Damage Reduction",
                    `At level 7, ${getIndefiniteArticle(vars.name)} ${vars.name} gains damage reduction. Subtract 1 damage from
                    the damage taken if dealt by a weapon or a natural attack. This reduction increases by 1 for every three levels thereafter
                    (13th, 16th, 19th). It cannot reduce the damage below 0.
                    `,
                    7
                ]);
            break;
            case "f-roguefeats":
                featuresarray.push([
                    "Trapfinding",
                    `${getIndefiniteArticle(vars.name)} ${vars.name} can use the search skill to find traps with a DC greater than 20.
                    <br>
                    <br>
                    ${getIndefiniteArticle(vars.name)} ${vars.name} can use the disable device skill to disable traps with a DC greater than 20.
                    <br>
                    <br>
                    If ${getIndefiniteArticle(vars.name)} ${vars.name} beats the trap's DC by 10 or more with a disable device check, can bypass the
                    trap (with her party) without needing to disarm it.
                    `,
                    1
                ]);
            break;
            case "f-fighterfeats":  

            featuresarray.push([
                "Bonus Combat Feats",
                `
                ${getIndefiniteArticle(vars.name, true)} ${vars.name} gets bonus combat-oriented feats every few
                levels as noted on the level advancement table above. These feats must be drawn from the fighter bonus feats list.
                Prerequisites for these still apply.
                `,
                1
            ]);  
            break;
            case "f-weaponmastery":
                featuresarray.push([
                    "Weapon of Choice",
                    `${getIndefiniteArticle(vars.name, true)} ${vars.name} chooses a weapon. This weapon must be wielded
                    to gain weapon mastery benefits from. This only applies to melee or thrown weapons.
                    `,
                    1
                ]);

                featuresarray.push([
                    "Ki Damage",
                    `While wielding a chosen weapon of choice (see feat), ${getIndefiniteArticle(vars.name)} ${vars.name}
                    can choose to deal maximum damage when striking an opponent. This move can be performed once per rest per level in ${vars.name}.
                    `,
                    1
                ]);

                featuresarray.push([
                    "Superior Weapon Focus",
                    `Starting from level 5 and every five levels thereafter, the ${vars.name} gains 
                    +1 bonus to all attack rolls with their weapon of choice. This stacks with any other weapon focus feats.
                    `,
                    5
                ]);

                featuresarray.push([
                    "Increased Multiplier",
                    `From level 7 and while wielding a chosen weapon of choice (see feat), the critical damage multiplier of the ${vars.name}'s weapon increases by x1.
                    For example, a x2 critical multiplier becomes a x3.
                    `,
                    7
                ]);

                featuresarray.push([
                    "Ki Critical",
                    `From level 10, the critical threat range of a chosen weapon wielded by the ${vars.name} increases
                    by +2. For example, a threat range of 19-20 becomes 17-20. A threat range of 20 becomes 18-20, and a threat range of 18-20 becomes 16-20.
                    This stacks with other feats that alter the threat range.
                    `,
                    10
                ]);
            break;
            case "f-magicfeats":
                featuresarray.push([
                    "Bonus Magic Feats",
                    `
                    ${getIndefiniteArticle(vars.name, true)} ${vars.name} gets bonus magic-oriented feats every few
                    levels as noted on the level advancement table above. These feats can be either metamagic feats, item creation feats, or
                    spell mastery. Prerequisites for these still apply.
                    `,
                    1
                ]);  
            break;
            case "f-favouredenemy":
                featuresarray.push([
                    "Track",
                    `
                    ${getIndefiniteArticle(vars.name, true)} ${vars.name} gains Track as a bonus feat.
                    `,
                    1
                ]);

                featuresarray.push([
                    "Favoured Enemy",
                    `
                    ${getIndefiniteArticle(vars.name, true)} ${vars.name} gains a favoured enemy, which they must select from
                    the table below. At 5th level and every five levels thereafter, another favoured enemy is gained.
                    <br><b>Favoured Enemy Bonus:</b> +2 bonus to bluff, listen, sense motive, spot, survival checks, and damage rolls vs the favoured enemy.
                    <br>
                    <br>
                    <table class="table table-sm table-light table-striped table-bordered w-auto">
                    <thead><tr>
                        <th>Type (subtype)</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr></thead>
                    <tbody>
                        <tr>
                            <td>Aberration</td>
                            <td>Humanoid (dwarf)</td>
                            <td>Humanoid (reptilian)</td>
                            <td>Outsider (fire)</td>
                        </tr>
                        <tr>
                            <td>Animal</td>
                            <td>Humanoid (elf)</td>
                            <td>Magical Beast</td>
                            <td>Outsider (good)</td>
                        </tr>
                        <tr>
                            <td>Construct</td>
                            <td>Humanoid (goblinoid)</td>
                            <td>Monstrous Humanoid</td>
                            <td>Outsider (lawful)</td>
                        </tr>
                        <tr>
                            <td>Dragon</td>
                            <td>Humanoid (gnoll)</td>
                            <td>Ooze</td>
                            <td>Outsider (native)</td>
                        </tr>
                        <tr>
                            <td>Elemental</td>
                            <td>Humanoid (gnome)</td>
                            <td>Outsider (air)</td>
                            <td>Outsider (water)</td>
                        </tr>
                        <tr>
                            <td>Fey</td>
                            <td>Humanoid (halfling)</td>
                            <td>Outsider (chaotic)</td>
                            <td>Plant</td>
                        </tr>
                        <tr>
                            <td>Giant</td>
                            <td>Humanoid (human)</td>
                            <td>Outsider (earth)</td>
                            <td>Undead</td>
                        </tr>
                        <tr>
                            <td>Humanoid (aquatic)</td>
                            <td>Humanoid (orc)</td>
                            <td>Outsider (evil)</td>
                            <td>Vermin</td>
                        </tr>
                    </tbody>
                    </table>
                    `,
                    1
                ]); 
            break;
            case "f-naturewalk":
                featuresarray.push([
                    "Woodland Stride",
                    `
                    Starting at level 2, ${getIndefiniteArticle(vars.name)} ${vars.name} may move through any sort of undergrowth
                    (e.g. natural thorns, briars, overgrown areas) at normal speed without taking damage or other impairments.
                    However, magically manipulated undergrowth still impedes motion.
                    `,
                    2
                ]);
                featuresarray.push([
                    "Trackless Step",
                    `
                    Starting at level 3, ${getIndefiniteArticle(vars.name)} ${vars.name} leaves no trail in natural surroundings
                    and cannot be tracked. She may choose to leave a trail if so desired.
                    `,
                    3
                ]);

                if (vars.features[i][1] == "2") {
                    featuresarray.push([
                        "Camouflage",
                        `
                        ${getIndefiniteArticle(vars.name, true)} ${vars.name} of 13th level or higher can use the hide skill in any sort
                        of natural terrain, even if the terrain doesn't grant cover or concealment.
                        `,
                        13
                    ]);
                    featuresarray.push([
                        "Hide in Plain Sight (nature)",
                        `
                        While in any sort of natural terrain, ${getIndefiniteArticle(vars.name)} ${vars.name} of 17th level or higher
                        can use the hide skill even while being observed.
                        `,
                        17
                    ]);                    
                }
            break;
            case "f-bardsong":
                featuresarray.push([
                    `${vars.name} Knowledge`,
                    `
                    ${getIndefiniteArticle(vars.name, true)} ${vars.name} may make a special knowledge check with a bonus
                    equal to his ${vars.name} level + his Intelligence modifier to know some relevant information about local notable
                    people, legendary items, or noteworthy places. (If the ${vars.name} has 5 or more ranks in knowledge (history), he gains
                    a +2 bonus on this check).
                    <br>
                    <br>
                    A successful check will not reveal the powers of a magic item but may hint to its general function.
                    ${getIndefiniteArticle(vars.name, true)} ${vars.name} may not take 10 or 20 on this; it is essentially random knowledge.
                    <br>
                    <table class="table table-sm table-light table-striped table-bordered w-auto">
                    <thead><tr>
                        <th>DC</th>
                        <th>Type of Knowledge</th>
                    </tr></thead>
                    <tbody>
                        <tr>
                            <td>10</td>
                            <td>Common, known by at least a substantial minority of the local population.</td>
                        </tr>
                        <tr>
                            <td>20</td>
                            <td>Uncommon but available, known by only a few people legends.</td>
                        </tr>
                        <tr>
                            <td>25</td>
                            <td>Obscure, known by few, hard to come by.</td>
                        </tr>
                        <tr>
                            <td>30</td>
                            <td>Extremely obscure, known by very few, possibly forgotten by most who once knew it,
                            possibly known only by those who don’t understand the significance of the knowledge.</td>
                        </tr>
                    </tbody>
                    </table>
                    `,
                    1
                ]);

                if (vars.features[i][1] == "2") {
                    featuresarray.push([
                        `${vars.name} Music`,
                        `Once per day per ${vars.name} level, ${getIndefiniteArticle(vars.name)} ${vars.name} can use his song, poetics, or music
                        to produce magical effects on those around him (including himself if desired). Each ability has a minimum level and perform ranks.
                        <br>
                        <br>
                        Performing is a standard action. Some performances require concentration (standard action per round). While performing,
                        ${getIndefiniteArticle(vars.name)} ${vars.name} cannot cast spells, activate magic items (e.g scrolls & wands), or use command words.
                        There's a 20% chance to fail if the ${vars.name} is deaf. Failure still counts against the daily limit.
                        `,
                        1
                    ]);
                    featuresarray.push([
                        "Countersong",
                        `<b>Perform:</b> 3 or more ranks <br>
                        ${getIndefiniteArticle(vars.name, true)} ${vars.name} can use their song to counter magical effects.
                        Enemy spellcasters within 30 feet of him take a 20% spell failure chance when casting any spell with a verbal component.
                        `,
                        1
                    ]);
                    featuresarray.push([
                        "Fascinate",
                        `<b>Perform:</b> 3 or more ranks <br>
                        <b>Number of Targets:</b> 1 per 3 ${vars.name} levels <br>
                        <b>DC:</b> Will save vs ${vars.name} perform check<br>
                        ${getIndefiniteArticle(vars.name, true)} ${vars.name} can use their song to fascinate one or more creatures within
                        90 feet who can see and hear the ${vars.name}, and can pay attention to him. The ${vars.name} must be able to see the creature.
                        `,
                        1
                    ]);
                    featuresarray.push([
                        "Inspire Courage",
                        `
                        <b>Perform:</b> 3 or more ranks <br>
                        <b>Duration:</b> For as long as the ally can hear, and 5 rounds after <br>
                        <b>Bonus:</b> +1 morale bonus on saving throws vs charm and fear, and bonus on attack and damage rolls <br>
                        ${getIndefiniteArticle(vars.name, true)} ${vars.name} can use their song to inspire courage in their allies (including themself).
                        The bonuses increases to +2 at level 8, and by 1 every 6 levels after. This is a mind-affecting ability.
                        `,
                        1
                    ]);
                    featuresarray.push([
                        "Inspire Competence",
                        `
                        <b>Perform:</b> 6 or more ranks <br>
                        <b>Duration:</b> For as long as the ally can hear, and 5 rounds after <br>
                        <b>Bonus:</b> +2 competence bonus on skill checks with a particular skill <br>
                        ${getIndefiniteArticle(vars.name, true)} ${vars.name} can use their song to help an ally complete a task.
                        The effect lasts as long as the bard concentrates, up to 2 minutes. The ${vars.name} cannot inspire himself.
                        `,
                        3
                    ]);
                    featuresarray.push([
                        "Suggestion",
                        `
                        <b>Perform:</b> 9 or more ranks <br>
                        <b>DC:</b> Will save vs 10 + 1/2 ${vars.name}'s level + ${vars.name}'s Cha modifier <br>
                        <b>Targets:</b> One creature <br>
                        ${getIndefiniteArticle(vars.name, true)} ${vars.name} can make a suggestion (as the spell) to a creature
                        that he has already fascinated. Using this ability does not break the ${vars.name}'s concentration on the fascinate effect,
                        nor allows a second saving throw against the fascinate effect.
                        <br>
                        <br>
                        Suggestion does not count towards ${getIndefiniteArticle(vars.name, true)} ${vars.name}'s daily music limit.
                        This is an enchantment (compulsion), mind-affecting, language dependent ability.
                        `,
                        6
                    ]);
                    featuresarray.push([
                        "Inspire Greatness",
                        `
                        <b>Perform:</b> 12 or more ranks <br>
                        <b>Bonus:</b> +2 hit dice (d10s), 2d10+target's Con modifier temporary hitpoints, +2 competence bonus on attack rolls, +1 competence bonus on Fortitude saves <br>
                        <b>Targets:</b> One creature (including himself) per 3 ${vars.name} levels beyond 9th <br>
                        <b>Duration:</b> Duration of the music and 5 rounds after <br>
                        ${getIndefiniteArticle(vars.name, true)} ${vars.name} can use their song to inspire greatness in one or more creatures (including himself).
                        The bonus hit dice counts as regular hit dice for determining spell effects that are hit dice dependant. (e.g. A level 8 wizard will cast a fireball as a 10d6 instead of 8d6)
                        This is a mind affecting ability.
                        `,
                        9
                    ]);
                    featuresarray.push([
                        "Song of Freedom",
                        `
                        <b>Perform:</b> 15 or more ranks <br>
                        <b>Duration:</b> 1 minute uninterrupted concentration and music <br>
                        <b>Range:</b> 30 feet <br>
                        <b>Targets:</b> One target (not the ${vars.name}) <br>
                        ${getIndefiniteArticle(vars.name, true)} ${vars.name} can use their song to create an effect
                        equivalent to the break enchantment spell.
                        `,
                        12
                    ]);
                    featuresarray.push([
                        "Inspire Heroics",
                        `
                        <b>Perform:</b> 18 or more ranks <br>
                        <b>Bonus:</b> +4 morale bonus to saving throws, +4 dodge bonus to AC <br>
                        <b>Targets:</b> One creature (including himself) per 3 ${vars.name} levels beyond 15th <br>
                        <b>Duration:</b> Duration of the music and 5 rounds after <br>
                        <b>Range:</b> 30 feet <br>
                        ${getIndefiniteArticle(vars.name, true)} ${vars.name} can use their song to instill heroism in their allies.
                        This is a mind affecting ability.
                        `,
                        15
                    ]);
                    featuresarray.push([
                        "Mass Suggestion",
                        `
                        <b>Perform:</b> 21 or more ranks <br>
                        This upgrades the suggestion ability to work on any number of creatures affected by fascinate.
                        `,
                        18
                    ]);
                }
            break;
            case "f-pugilism":
                featuresarray.push([
                    "Unarmed Strike",
                    `${getIndefiniteArticle(vars.name, true)} ${vars.name} gains improved unarmed strike as a bonus feat.
                    Any body part is counted for an unarmed strike. There is no offhand for unarmed attacks, and a full strength bonus can be applied
                    to damage rolls.
                    <br>
                    <br>
                    ${getIndefiniteArticle(vars.name, true)} ${vars.name} can choose to deal lethal or nonlethal damage. This includes grappling.
                    Her unarmed strike is considered both a manufactured and natural weapon for the purposes of spells and effects that enhance weapons.
                    <br>
                    <br>
                    Extra damage is also gained for unarmed strikes:
                    <table class="table table-sm table-light table-striped table-bordered w-auto">
                    <thead><tr>
                        <th>Level</th>
                        <th>Damage (small ${vars.name})</th>
                        <th>Damage (medium ${vars.name})</th>
                        <th>Damage (large ${vars.name})</th>
                    </tr></thead>
                    <tbody>
                        <tr>
                            <td>1st-3rd</td>
                            <td>1d4</td>
                            <td>1d6</td>
                            <td>1d8</td>
                        </tr>
                        <tr>
                            <td>4th-7th</td>
                            <td>1d6</td>
                            <td>1d8</td>
                            <td>2d6</td>
                        </tr>
                        <tr>
                            <td>8th-11th</td>
                            <td>1d8</td>
                            <td>1d10</td>
                            <td>2d8</td>
                        </tr>
                        <tr>
                            <td>12th-15th</td>
                            <td>1d10</td>
                            <td>2d6</td>
                            <td>3d6</td>
                        </tr>
                        <tr>
                            <td>16th-19th</td>
                            <td>2d6</td>
                            <td>2d8</td>
                            <td>3d8</td>
                        </tr>
                        <tr>
                            <td>20th</td>
                            <td>2d8</td>
                            <td>2d10</td>
                            <td>4d8</td>
                        </tr>
                    </tbody>
                    </table>
                    `,
                    1
                ]);
                featuresarray.push([
                    "Ki Strike",
                    `${getIndefiniteArticle(vars.name, true)} ${vars.name}'s unarmed attacks are empowered by ki.
                    Her unarmed attacks are considered magic weapons for the purposes of ignoring damage reduction.
                    At level 10 they are also considered lawful, and at level 16 they are considered adamantine.
                    `,
                    4
                ]);

                if (vars.features[i][1] == "2") {
                    featuresarray.push([
                        "Flurry of Blows",
                        `When unarmoured and unencumbered, ${getIndefiniteArticle(vars.name)} ${vars.name} may make one extra attack
                        in a round at her highest base attack bonus, albeit with a -2 penalty to all attacks.
                        `,
                        1
                    ]);
                    featuresarray.push([
                        "Greater Flurry",
                        `At 11th level, the ${vars.name}'s flurry of blows is upgraded. A second extra attack is available.
                        `,
                        11
                    ]);
                    featuresarray.push([
                        "Quivering Palm",
                        `${getIndefiniteArticle(vars.name, true)} ${vars.name} can set up vibrations in the body of another creature
                        which can be fatal if desired. It can be used once per week, and must be announced before the attack roll.
                        <br>
                        <br>
                        Creatures immune to critical hits or with no discernable anatomy are immune. If successfully striking the creature,
                        the ${vars.name} can kill the victim at any point in time within a number of days equal to ${vars.name} levels.
                        If the target fails a fortitude save (DC: 10 + 1/2 ${vars.name}'s level + ${vars.name}'s Wis modifier) it dies. If it succeeds,
                        the target is no longer in danger, but can be affected by another quivering palm in the future.
                        `,
                        15
                    ]);
                }
            break;
            case "f-channel":
                let foe_channel = vars.features[i][2][0];
                featuresarray.push([
                    `Turn or Rebuke ${foe_channel}`,
                    `
                    <b>Times per day:</b> 3 + Charisma modifier
                    <br>
                    <b>Requires:</b> Holy or unholy symbol of the ${vars.name}'s deity
                    <br>
                    <br>
                    ${getIndefiniteArticle(vars.name, true)} ${vars.name} has the power to affect ${foe_channel}
                    by channeling the power of his faith through his holy (or unholy) symbol.
                    ${getIndefiniteArticle(vars.name, true)} ${vars.name} with a good deity can turn or destroy ${foe_channel}.
                    ${getIndefiniteArticle(vars.name, true)} ${vars.name} with an evil deity can turn or destroy ${foe_channel}.
                    ${getIndefiniteArticle(vars.name, true)} ${vars.name} with a neutral deity must choose good or evil functionality, and once chosen, cannot be changed.
                    ${getIndefiniteArticle(vars.name, true)} ${vars.name} with 5 or more ranks in Knowledge (religion) gets +2 bonus to turn checks.
                    `,
                    1
                ]);
            break;
            case "f-smite":
                let foe_smite = vars.features[i][2][0];
                featuresarray.push([
                    `Smite ${foe_smite}`,
                    `
                    <b>Extra Damage:</b> 1 per ${vars.name} level
                    <br>
                    <br>
                    Once per day, ${getIndefiniteArticle(vars.name)} ${vars.name} may attempt to smite ${foe_smite} with one normal melee attack.
                    by channeling the power of his faith through his holy (or unholy) symbol.
                    She adds her Charisma bonus (if any) to her attack roll and deals 1 point of damage per ${vars.name} level.
                    If the ${vars.name} smites a non-${foe_smite} creature, the smite has no effect, but the ability is still used up.
                    `,
                    1
                ]);
            break;
            case "f-domains":
                let value_domains = vars.features[i][1];
                let domains = (value_domains > 1) ? "domains" : "domain";

                featuresarray.push([
                    "Domains",
                    `${getIndefiniteArticle(vars.name, true)} ${vars.name} chooses ${value_domains} ${domains} from those
                    belonging to his deity. If he has particular deity, the ${domains} should represent their inclinations and abilities.
                    Alignment domains (Chaos, Lawful, Evil, Good) are only allowed if they match the ${vars.name}'s alignment.
                    <br>
                    <br>
                    Each domain gives the ${vars.name} access to a domain spell at each spell level (use the Cleric spell progression), as well as
                    a domain specific power.
                    `,
                    1
                ]);
            break;
            case "f-health":
                
                let health_choice = vars.features[i][3];
                let disease = (health_choice.toLowerCase() == "disease immunity");
                let poison = (health_choice.toLowerCase() == "poison immunity");
                
                if (vars.features[i][1] == "2" || disease) {
                    featuresarray.push([
                        "Disease Immunity",
                        `At 4th level, ${getIndefiniteArticle(vars.name)} ${vars.name} gains immunity to all diseases.
                        `,
                        4
                    ]);                   
                }
                if (vars.features[i][1] == "2" || poison) {
                    featuresarray.push([
                        "Venom Immunity",
                        `At 9th level, ${getIndefiniteArticle(vars.name)} ${vars.name} gains immunity to all poisons.
                        `,
                        9
                    ]);
                }
                    

            break;
            case "f-enlightenment":
                featuresarray.push([
                    "Still Mind",
                    `At 3rd level, ${getIndefiniteArticle(vars.name)} ${vars.name} gains +2 bonus on saving throws vs enchantment school spells and effects.
                    `,
                    3
                ]);
                if (vars.features[i][1] > 1) {
                    featuresarray.push([
                        "Wholeness of Body (Su)",
                        `At 7th level, ${getIndefiniteArticle(vars.name)} ${vars.name} can heal her own wounds. She can heal hitpoints equal to twice her ${vars.name} level per day. This can be spread over several uses.
                        `,
                        7
                    ]);
                }
                if (vars.features[i][1] > 2) {
                    featuresarray.push([
                        "Diamond Soul",
                        `At 13th level, ${getIndefiniteArticle(vars.name)} ${vars.name} gains spell resistance equal to 10 + ${vars.name} level. To affect the ${vars.name}, a spellcaster must
                        make a caster level check (1d20 + caster level) vs spell resistance (equal or exceeds).
                        `,
                        13
                    ]);
                    featuresarray.push([
                        "Empty Body",
                        `At 19th level, ${getIndefiniteArticle(vars.name)} ${vars.name} can become ethereal for 1 round per ${vars.name} level per day (as per the spell Etherealness). This can be spread over several uses.
                        `,
                        19
                    ]);
                    featuresarray.push([
                        "Perfect Self",
                        `At 20th level, ${getIndefiniteArticle(vars.name)} ${vars.name} becomes a magical creature. They are considered an outsider for the purpose of spells and magical effects.
                        They also gain 10/magic damage reduction. Unlike other outsiders, they can still be brought back from the dead.
                        `,
                        20
                    ]);
                }
            break;
            case "f-reaction":
                featuresarray.push([
                    "Evasion",
                    `At 2nd level, whenever a reflex save is allowed for half damage, ${getIndefiniteArticle(vars.name)} ${vars.name} 
                    instead takes no damage if he succeeds at the save.
                    `,
                    2
                ]);

                if (vars.features[i][1] > 1) {
                    featuresarray.push([
                        "Improved Evasion",
                        `At 9th level, whenever a reflex save is allowed for half damage, ${getIndefiniteArticle(vars.name)} ${vars.name} 
                        instead takes no damage if he succeeds at the save. Even if he fails the save, he takes only half damage.
                        `,
                        9
                    ]);
                }
            break;
            case "f-speed":
                if (vars.features[i][1] == 1) {
                    featuresarray.push([
                        "Fast Movement",
                        `${getIndefiniteArticle(vars.name, true)} ${vars.name}'s land speed is faster than normal by +10 feet.
                        This benefit only applies when he is wearing no armour, light armour, or medium armour and not carrying a heavy load.
                        It is applied before any load or armour modification.
                        `,
                        1
                    ]);
                }
                if (vars.features[i][1] == 2) {
                    featuresarray.push([
                        "Fast Movement",
                        `At 3rd level, ${getIndefiniteArticle(vars.name)} ${vars.name} gains an enhancement bonus to movement, as shown in
                        the table above. Wearing armour or having a medium or heavy load negates this bonus.
                        `,
                        3
                    ]);

                    featuresarray.push([
                        "Slow Fall",
                        `Starting at 4th level, ${getIndefiniteArticle(vars.name)} ${vars.name} within arm's reach of a wall can use it to slow their descent.
                        It reduces fall damage as if the fall was 20 feet shorter than it actually is. This bonus increases until 20th level, where she can use the 
                        wall to slow the descent at any distance.
                        `,
                        4
                    ]);
                }
            break;
            case "f-awareness":
                featuresarray.push([
                    "Uncanny Dodge",
                    `At 2nd level, ${getIndefiniteArticle(vars.name)} ${vars.name} retains their dexterity bonus to AC (if any)
                    if ever caught flatfooted or struck by an invisible attacker. This is lost if immobilised. If ${getIndefiniteArticle(vars.name)} ${vars.name}
                    already has uncanny dodge, it becomes improved uncanny dodge.
                    `,
                    2
                ]);
                featuresarray.push([
                    "Improved Uncanny Dodge",
                    `At 5th level, ${getIndefiniteArticle(vars.name)} ${vars.name} can no longer be flanked. This denies being sneak attacked,
                    unless the attacker is at least 4 levels greater than ${vars.name} levels. If he gained improved uncanny dodge from another class,
                    those levels stack with his ${vars.name} levels to determine flankability.
                    `,
                    5
                ]);
                featuresarray.push([
                    "Trap Sense",
                    `Starting at 3rd level, ${getIndefiniteArticle(vars.name)} ${vars.name} gains +1 bonus on Reflex saves vs traps and a +1 dodge bonus to AC vs traps.
                    These bonuses increase every 3 ${vars.name} levels thereafter. It stacks with trap sense gained from other classes.
                    `,
                    3
                ]);
            break;
            case "f-holygifts":
                featuresarray.push([
                    "Detect Evil",
                    `At will, ${getIndefiniteArticle(vars.name)} ${vars.name} can use detect evil, as the spell.
                    `,
                    1
                ]);
                featuresarray.push([
                    "Divine Grace",
                    `At 2nd level, ${getIndefiniteArticle(vars.name)} ${vars.name} gains a bonus equal to their Charisma bonus (if any)
                    on all saving throws.
                    `,
                    2
                ]);
                featuresarray.push([
                    "Lay On Hands",
                    `At 2nd level, ${getIndefiniteArticle(vars.name)} ${vars.name} with 12 or more Charisma can heal wounds (her or others) by touch.
                    She can heal up to her ${vars.name} level x her Charisma bonus per day. Can be split over multiple casts. It is a standard action.
                    If another class grants Lay On Hands, both classes stack to determine heal amount.
                    `,
                    2
                ]);
                featuresarray.push([
                    "Aura Of Courage",
                    `Beginning 3rd level, ${getIndefiniteArticle(vars.name)} ${vars.name} is immune to fear (magic or otherwise).
                    Each ally within 10 feet gains a +4 morale bonus on saving throws against fear effects. Does not function when she is
                    unconscious or dead.
                    `,
                    3
                ]);
                featuresarray.push([
                    "Remove Disease",
                    `Beginning 6th level, ${getIndefiniteArticle(vars.name)} ${vars.name} can cast remove disease, as per the spell, once per week.
                    Every three levels after 6th gives an additional casting.
                    `,
                    6
                ]);
            break;
            case "f-unholygifts":
                featuresarray.push([
                    "Detect Good",
                    `At will, ${getIndefiniteArticle(vars.name)} ${vars.name} can use detect good, as the spell.
                    `,
                    1
                ]);
                featuresarray.push([
                    "Poison Use",
                    `${getIndefiniteArticle(vars.name, true)} ${vars.name} is skilled in the use of poison and does not require
                    a check when applying or creating it.
                    `,
                    1
                ]);
                featuresarray.push([
                    "Dark Blessing",
                    `At 2nd level, ${getIndefiniteArticle(vars.name)} ${vars.name} gains a bonus equal to their Charisma bonus (if any)
                    on all saving throws.
                    `,
                    2
                ]);
                featuresarray.push([
                    "Aura Of Despair",
                    `Beginning 3rd level, ${getIndefiniteArticle(vars.name)} ${vars.name} radiates a malign aura that cause enemies within
                    10 feet to take a -2 penalty on all saving throws.
                    `,
                    3
                ]);
            break;
            case "f-animalcompanion":

                let type_animalcompanion = (vars.features[i][1] == 3)
                    ? `${vars.features[i][2][0]}`.toLowerCase()
                    : `animal`;

                let name_animalcompanion = `${capitalise(type_animalcompanion)} Companion`;

                let intro_animalcompanion;
                let level_animalcompanion;
                if (vars.features[i][1] == 1) {
                    intro_animalcompanion = `At 4th level, ${getIndefiniteArticle(vars.name)} ${vars.name}`;
                    level_animalcompanion = 4;
                } else {
                    intro_animalcompanion = `${getIndefiniteArticle(vars.name, true)} ${vars.name}`;
                    level_animalcompanion = 1;
                }

                    featuresarray.push([
                        name_animalcompanion,
                        `${intro_animalcompanion} gains ${getIndefiniteArticle(type_animalcompanion)} ${type_animalcompanion} companion. 
                        This can be any medium or smaller ${type_animalcompanion}. Depending on the campaign and/or dm ruling, a large sized ${type_animalcompanion}
                        may be available. The ${type_animalcompanion} companion gains abilities as it levels up alongside the ${vars.name}.
                        `,
                        level_animalcompanion
                    ]);
            break;
            case "f-familiar":
                let type_familiar = (vars.features[i][1] == 2)
                ? `${vars.features[i][2][0]}`.toLowerCase()
                : `animal`;

                let name_familiar = `${capitalise(type_familiar)} Familiar`;

                let extra_familiar = (vars.features[i][1] == 2)
                ? ""
                : "It is treated as a magical beast for the purpose of any effect that depends on its type.";

                featuresarray.push([
                    name_familiar,
                    `${getIndefiniteArticle(vars.name, true)} ${vars.name} gains ${getIndefiniteArticle(type_familiar)} ${type_familiar} familiar. 
                    This can be any small or tiny ${type_familiar}. Depending on the campaign and/or dm ruling, a medium sized ${type_familiar}
                    may be available. The ${type_familiar} familiar gains abilities as it levels up alongside the ${vars.name}.
                    ${extra_familiar}
                    `,
                    1
                ]);
            break;
            case "f-wildshape":
                let shape1 = vars.features[i][2][0];
                let shape2 = vars.features[i][2][1];
                let shape3 = vars.features[i][2][2];

                let stats_wildshape = (vars.features[i][1] == 1)
                    ? ""
                    : `<br><b>Large form unlocked:</b> 8th level <br>
                    <b>Tiny form unlocked:</b> 11th level <br>
                    <b>Huge form unlocked:</b> 15th level (20th level for ${shape3} shape)`;

                let size_wildshape = (vars.features[i][1] == 1)
                    ? "any Small"
                    : "any Small or Medium";

                featuresarray.push([
                    "Wild Shape",
                    `
                    <b>Duration:</b> 1 hour per ${vars.name} level <br>
                    <b>Action:</b> 1 standard action <br>
                    <b>Second shape:</b> ${shape2}, level 12 <br>
                    <b>Third shape:</b> ${shape3}, level 16
                    ${stats_wildshape}
                    <br>
                    At 5th level, ${getIndefiniteArticle(vars.name)} ${vars.name} gains the ability to turn herself into ${size_wildshape}
                    ${shape1} and back again once per day. It includes all creatures of the ${shape1} type. When wild shape is used, she regains
                    lost hitpoints as if resting for a night. Any gear worn or carried melds into the new form and becomes non-functional. 
                    <br>
                    <br>
                    The ${vars.name} must be familiar with the creature they intend to change into. Communication is restricted to that which is available
                    to the shape. The new form hit points cannot exceed the character's hit points.
                    `,
                    5
                ]);
            break;
        }
    }

    $(".template-features").html(sortDescriptions(featuresarray));
}

function buildCastingTables() {

}

function capitalise(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}