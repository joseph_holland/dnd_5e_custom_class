var pointstotal = $("#pointstotal");
var pointsused = $("#pointsused");
var pointsremaining = $("#pointsremaining");

var name = "";
var description = "";
var bab = "";
var hd = "";
var saves = "";
var skillpoints = "";
var skills = "";
var weaponprof = "";
var armourprof = "";
var shieldprof = "";
var equiprestrict = "";
var alignrestrict = "";

var features = new Map();

var lastSaveToggled = null

// --- UTILITY -----------------------------------------------------

function resetTool() {
    $('select').each(function() {
        $(this).val("0").change();
    });
    $("input").val("");
    $("input:checkbox").prop("checked", false);
    updateDialog();
    pointstotal.val(25);
    pointsused.val(0);
    pointsremaining.val(25);

    name = "";
    description = "";
    bab = "";
    hd = "";
    saves = "";
    skillpoints = "";
    skills = "";
    weaponprof = "";
    armourprof = "";
    shieldprof = "";
    equiprestrict = "";
    alignrestrict = "";
    lastSaveToggled = null
    features = new Map();

    resetSkillsList();
    buildFeatureDropdowns();
}

function openMenu(name) {
    // open the corresponding option table, and set button to red/white
    $("#"+name).toggleClass("d-none");
    $("#button-"+name).toggleClass("btn-light");
    $("#button-"+name).toggleClass("btn-danger");
}

function updatePoints() {
    // update the points counter in the sidebar
    var num = 0;
    $('select.counter').each(function() {
        num += parseInt($(this).val());
    });

    pointsused.val(num);
    pointsremaining.val(parseInt(pointstotal.val()) - num);
}

function updateDialog() {
    // set the name and description
    $("#preview-name").text(name);
    $("#preview-description").text(description);
    
    // set all the abilities
    // TODO: change to dynamic like for features
    getStats();
    $("#preview-bab").text(bab);
    $("#preview-hd").text(hd);
    $("#preview-saves").text(saves);
    $("#preview-skillpoints").text(skillpoints);
    $("#preview-skills").text(skills);
    $("#preview-weaponprof").text(weaponprof);
    $("#preview-armourprof").text(armourprof);
    $("#preview-shieldprof").text(shieldprof);
    $("#preview-equiprestrict").text(equiprestrict);
    $("#preview-alignrestrict").text(alignrestrict);
    
    // set all the features
    $(".preview-features").empty();
    getFeatures();
    var featureList = [];
    for (const [key,value] of features.entries()) {
        featureList +=  `<div class="container-fluid justify-content-left">
                            <div class="row">
                                <strong>${key}:&nbsp</strong>
                                <div>${value}</div>
                            </div>
                        </div>`;
    };
    $(".preview-features").html(featureList);
}

function applyBoxModifiers(boxParentId) {
    // when a writebox becomes visible, it may have special clauses
    var val = $(`#${boxParentId}`).next()
                        .find(".writebox :selected")
                        .val();
    var selected = "";

    $(`#${boxParentId}`).parent().find(".writebox-popup select").each(function(){
        $(this).find("option").each(function() {
            $(this).removeClass("d-none");
            if ( $(this).parent().hasClass("f-all") || $(this).hasClass(`write-${val}`)) {
                $(this).removeClass("d-none");
                if ($(this).is(":selected")) {selected = $(this).val()}
            }
            else {
                $(this).addClass("d-none");
            }
        }
        )}
    );
    $(`#${boxParentId}`).parent().find(".writebox-popup select").val(selected);
}

function setDescription() {
    description = $("#description textarea").val();
}

// --- SKILLS ------------------------------------------------------

function skillSwitch(skillName) {
    // for switching the skill labels from the list to picked and back
    var picked = $(".ability-skills-picked");
    var remaining = $(".ability-skills-remaining");
    var skill = $(`#skill-${skillName}`)

    if (skill.parent().hasClass("ability-skills-picked")) {
        skill.appendTo(remaining);
        var buttons = $(".ability-skills-remaining :button").sort(function(a,b){
            return $(a).text() > $(b).text();
        });
        remaining.html(buttons);
    }
    else {
        if($(".ability-skills-picked :button").length < getNumberOfSkills()) {
            skill.appendTo(picked);
            var buttons = $(".ability-skills-picked :button").sort(function(a,b){
                return $(a).text() > $(b).text();
            });
            picked.html(buttons);
        }
    }
    updateSkills();
}

function updateSkills() {
    var maxSkills = getNumberOfSkills();
    var skillsToPick = maxSkills - $(".ability-skills-picked button").length;
    $("#skill-skillsRemaining").val(skillsToPick);

    if($(".ability-skills-picked :button").length >= getNumberOfSkills()) {
        $(".maxSkillsReached").removeClass("d-none");
    }
    else {
        $(".maxSkillsReached").addClass("d-none"); 
    }
}

function getNumberOfSkills() {
    return parseInt($(".ability-skills select :selected").text());
}

function resetSkillsList() {
    var remaining = $(".ability-skills-remaining");
    $(".ability-skills-picked :button").each(function(){
        $(this).appendTo(remaining);
    });
    var buttons = $(".ability-skills-remaining :button").sort(function(a,b){
        return $(a).text() > $(b).text();
    });
    remaining.html(buttons);

    updateSkills();
}

// --- SAVES --------------------------------------------------------

function savesToggle(save) {
    // for toggling the saves checkboxes and handling 1 & 2 saves
    var save2;
    var save3;
    
    if (save == "") {
        var save1 = $("#ability-saves-toggle .save1"); 
        save2 = $("#ability-saves-toggle .save2"); 
        save3 = $("#ability-saves-toggle .save3"); 

        save1.prop("checked", false);
        save2.prop("checked", false);
        save3.prop("checked", false);
    }
    else {
        switch(save) {
            case "save1":
                save2 = $("#ability-saves-toggle .save2"); 
                save3 = $("#ability-saves-toggle .save3"); 
                break;
            case "save2":
                save2 = $("#ability-saves-toggle .save1"); 
                save3 = $("#ability-saves-toggle .save3");
                break; 
            case "save3":
                save2 = $("#ability-saves-toggle .save1"); 
                save3 = $("#ability-saves-toggle .save2"); 
        }

        var save2;
        var save3;
    
        var value = $("#ability-saves").val();
    
        switch(parseInt(value)) {
            case 1:
                save2.prop("checked", false);
                save3.prop("checked", false);
                break;
            case 2:
                var currentToggled = 0;
                currentToggled += (save2.prop("checked")) ? 1 : 0;
                currentToggled += (save3.prop("checked")) ? 1 : 0;
                if (currentToggled == 2) {
                    if (lastSaveToggled == null) {
                        lastSaveToggled = save3;
                    }
                    lastSaveToggled.prop("checked", false);
    
                    // if lastSavedToggle == x and save == y then set to z
                    lastSaveToggled = (lastSaveToggled.is(save2)) ? save3 : save2;
                }
            break;
        }
    }
}

// --- FEATURES -----------------------------------------------------

function getFeatures() {
    features = new Map();

    // loop through features
    $("#features .feature").each(function(){
        // get feature text
        var n = $(this).find("td.feature-name").text();
        // get feature selection text
        var d = "";
        $(this).find("td.feature-select :selected").each(
            function() {d += `${$(this).text()} `}
        );
        // if it's not 0 (aka None is selected in the box)
        if ($(this).find("td.feature-select :selected").val() != 0) {
            // find the writebox
            var writebox = $(this).find(".writebox-popup");
            // if writebox is visible (aka supposed to be written into)
            if (!writebox.hasClass("d-none")) {
                inputs = new Array();
                // get all writebox input values
                writebox.children("input").each(function() {
                    inputs.push($(this).val());
                })
                // if there are actually values
                if (inputs.length > 0) {
                    d += ` ${inputs.join(", ")}`;
                }
            }
            // add it to the features
            features.set(n, d);
        }
    });
}

function getSelectedFeaturesById() {
    // loops through all the features in the html and grabs the ones that have a selection
    // value greater than 0. 0 means None.
    
        var featureIds = [];
        $("#features .feature").each(function(){
    
            if ($(this).find("td.feature-select :selected").val() != 0) {
                featureIds.push($(this).find("td.feature-name").prop("id"));
            }
        });
        return featureIds;
}

function getFeatureLevel(level, feature, table) {
    // get features for each level depending on what feature is chosen
    // each feature is done differently. Most of them use a switch case to
    // give an updated feat per level. There's probably mathematical ways to do it in
    // less lines, but it's more readable in a switch case
    
    // it gets the row from the table that corresponds to the level and appends the
    // corresponding feat to it
    
    var value = $(`#${feature}`).next().find("select :selected").val();
    var row = table[level];

    switch(feature) {
        case "f-sneakattack":
            var num = 0;
            switch (parseInt(value)) {
                case 1:
                    num = 4;
                    break;
                case 2:
                    num = 3;
                    break;
                case 3:
                    num = 2;
                    break;
                default:
                    num = 0;
            }
            if (num) {
                if (level == 0) {
                    row.push("sneak attack +1d6");
                } else if (level % num == 0) {
                    row.push(`sneak attack +${level/num+1}d6`);
                }
            }
            break;
        case "f-arcanecast":
            if (level == 0) {
                row.push("arcane spellcasting");
            }
            break;
        case "f-divinecast":
            if (level == 0) {
                row.push("divine spellcasting");
            }
            switch($(".f-divinecast-box :selected").text().toUpperCase()){
                case "PALADIN":
                case "CLERIC":
                    row.push("aura");
                    break;
            }

            break;
        case "f-rage":
            switch(parseInt(level)) {
                case 0:
                    row.push("rage 1/day");
                    break;
                case 3:
                    row.push("rage 2/day");
                    break;
                case 7:
                    row.push("rage 3/day");
                    break;
                case 10:
                    row.push("greater rage");
                    break;
                case 11:
                    row.push("rage 4/day");
                    break;
                case 13:
                    row.push("indomitable will");
                case 15:
                    row.push("rage 5/day");
                break;
                case 16:
                    row.push("tireless rage");
                    break;
                case 19:
                    row.push("mighty rage");
                    row.push("rage 6/day");
                    break;
                default:
            }
        break;
        case "f-damagereduction":
            switch(parseInt(level)) {
                case 6:
                    row.push("damage reduction 1/--");
                    break;
                case 9:
                    row.push("damage reduction 2/--");
                    break;
                case 12:
                    row.push("damage reduction 3/--");
                    break;
                case 15:
                    row.push("damage reduction 4/--");
                    break;
                case 18:
                    row.push("damage reduction 5/--");
                    break;
                default:
            }
        break;
        case "f-roguefeats":
            if (level == 0) {
                row.push("trapfinding");
            }

            if (value == 2) {
                if (
                    parseInt(level) == 9 || parseInt(level) == 12 ||
                    parseInt(level) == 15 || parseInt(level) == 18
                    ) { 
                        row.push("special ability");
                    }
            }   
        break;
        case "f-fighterfeats":
            var num = 0;
            switch (parseInt(value)) {
                case 1:
                    num = 4;
                    break;
                case 2:
                    num = 3;
                    break;
                case 3:
                    num = 2;
                    break;
                default:
                    num = 0;
            }
            if (num) {
                if ( level == 0 || (level+1) % num == 0) {
                    row.push(`bonus combat feat`);
                }
            }           
        break;
        case "f-weaponmastery":
            switch(parseInt(level)) {
                case 0:
                    row.push("ki damage");
                    row.push("weapon of choice");
                    break;
                case 4:
                    row.push("superior weapon focus +1");
                    break;
                case 6:
                    row.push("increased multiplier");
                    break;
                case 9:
                    row.push("ki critical");
                    row.push("superior weapon focus +2");
                    break;
                case 14:
                    row.push("superior weapon focus +3");
                    break;
                case 19:
                    row.push("superior weapon focus +4");
                default:
            }
        break;
        case "f-magicfeats":
            var num = 0;
            switch (parseInt(value)) {
                case 1:
                    num = 6;
                    break;
                case 2:
                    num = 5;
                    break;
                case 3:
                    num = 4;
                    break;
                case 4:
                    num = 3;
                    break;
                default:
                    num = 0;
            }
            if (num) {
                if (level != 0 && (level+1) % num == 0) {
                    row.push(`bonus magic feat`);
                }
            }
        
        break;
        case "f-favouredenemy":
            switch(parseInt(level)) {
                case 0:
                    row.push("track");
                    row.push("1st favoured enemy");
                    break;
                case 4:
                    row.push("2nd favoured enemy");
                    break;
                case 9:
                    row.push("3rd favoured enemy");
                    break;
                case 14:
                    row.push("4th favoured enemy");
                    break;
                case 19:
                    row.push("5th favoured enemy");
                    break;
                default:
            }
        break;
        case "f-naturewalk":
            switch(parseInt(level)) {
                case 1:
                    row.push("woodland stride");
                    break;
                case 2:
                    row.push("trackless step");
                    break;
                default:
            }

            if (value == 2) {
                switch(parseInt(level)) {
                    case 12:
                        row.push("camouflage");
                        break;
                    case 16:
                        row.push("hide in plain sight");
                        break;
                    default:
                }
            }
        break;
        case "f-bardsong":
            if (level == 0) {
                row.push("bardic knowledge");
            }
    
            if (value > 1) {
                switch(parseInt(level)) {
                    case 0:
                        row.push("bardic music");
                        row.push("countersong");
                        row.push("fascinate");
                        row.push("inspire courage +1");
                        break;
                    case 2:
                        row.push("inspire competence");
                        break;
                    case 5:
                        row.push("suggestion");
                        break;
                    case 7:
                        row.push("inspire courage +2");
                        break;
                    case 8:
                        row.push("inspire greatness");
                        break;
                    case 11:
                        row.push("song of freedom");
                        break;
                    case 13:
                        row.push("inspire courage +3");
                        break;
                    case 14:
                        row.push("inspire heroics");
                        break;
                    case 17:
                        row.push("mass suggestion");
                        break;
                    case 19:
                        row.push("inspire courage +4");
                        break;
                    default:
                }
            }
        break;
        case "f-pugilism":
            switch(parseInt(level)) {
                case 0:
                    row.push("monk unarmed strike");
                    break;
                case 3:
                    row.push("ki strike (magic)");
                    break;
                case 9:
                    row.push("ki strike (lawful)");
                    break;
                case 15:
                    row.push("ki strike (adamantine)");
                    break;
                default:
            }

            if (value > 1) {
                switch(parseInt(level)) {
                    case 0:
                        row.push("flurry of blows");
                        break;
                    case 10:
                        row.push("greater flurry");
                        break;
                    case 14:
                        row.push("quivering palm");
                        break;
                    default:
                }
            }
        break;
        case "f-channel":
            if (level == 0) {
                var creature = $(".f-channel-box :selected").val();
                row.push(`turn, command, or rebuke ${creature}`);
            }
        break;
        case "f-smite":
            var foe = $(".f-smite-box :selected").val();
            switch(parseInt(level)) {
                case 0:
                    row.push(`smite ${foe} (1/day)`);
                    break;
                case 4:
                    row.push(`smite ${foe} (2/day)`);
                    break;
                case 9:
                    row.push(`smite ${foe} (3/day)`);
                    break;
                case 14:
                    row.push(`smite ${foe} (4/day)`);
                    break;
                case 19:
                    row.push(`smite ${foe} (5/day)`);
                    break;
            }
        break;
        case "f-domains":
            if (level == 0) {
                var string = value + " domain"
                if (value > 1) {string += "s"}
                row.push(string);
            }
        break;
        case "f-health":
            var valueClass = $(`#${feature}`).next().find("select :selected");
            if ((valueClass.hasClass("diseaseImmune") || value == 2) && level == 3) {
                row.push("disease immunity");
            }
            else if ((valueClass.hasClass("poisonImmune") || value == 2) && level == 8) {
                row.push("venom immunity");
            }
        break;
        case "f-enlightenment":
            if (level == 2) {
                row.push("still mind");
            }
            else if (level == 6 && value > 1) {
                row.push("wholeness of body");
            }
            else if (value > 2) {
                switch(parseInt(level)) {
                    case 12:
                        row.push("diamond soul");
                        break;
                    case 18:
                        row.push("empty body");
                        break;
                    case 19:
                        row.push("perfect self");
                        break;
                }
            }
        break;
        case "f-reaction":
            if (level == 1) {
                row.push("evasion");
            }
            else if (value == 2 && level == 8) {
                row.push("improved evasion");
            }
        break;
        case "f-speed":
            if (value == 1 && level == 0) {
                row.push("fast movement");
            }
            else if (value == 2) {
                switch(parseInt(level)) {
                    case 2:
                        row.push("unarmed speed bonus +10ft.");
                        break;
                    case 3:
                        row.push("slow fall 20ft.");
                        break;
                    case 5:
                        row.push("slow fall 30ft.");
                        row.push("unarmed speed bonus +20ft.");
                        break;
                    case 7:
                        row.push("slow fall 40ft.");
                        break;
                    case 8:
                        row.push("unarmed speed bonus +30ft.");
                        break;
                    case 9:
                        row.push("slow fall 50ft.");
                        break;
                    case 11:
                        row.push("slow fall 60ft.");
                        row.push("unarmed speed bonus +40ft.");
                        break;
                    case 13:
                        row.push("slow fall 70ft.");
                        break;
                    case 14:
                        row.push("unarmed speed bonus +50ft.");
                        break;
                    case 15:
                        row.push("slow fall 80ft.");
                        break;
                    case 17:
                        row.push("slow fall 90ft.");
                        row.push("unarmed speed bonus +60ft.");
                        break;
                    case 19:
                        row.push("slow fall any distance");
                        break;
                    default:
                }
            }
        break;
        case "f-awareness":
            if (value == 1) {
                switch(parseInt(level)) {
                    case 1:
                        row.push("uncanny dodge");
                        break;
                    case 2:
                        row.push("trap sense +1");
                        break;
                    case 4:
                        row.push("improved uncanny dodge");
                        break;
                    case 5:
                        row.push("trap sense +2");
                        break;
                    case 8:
                        row.push("trap sense +3");
                        break;
                    case 11:
                        row.push("trap sense +4");
                        break;
                    case 14:
                        row.push("trap sense +5");
                        break;
                    case 17:
                        row.push("trap sense +6");
                        break;
                }
            }
        break;
        case "f-holygifts":
            switch(parseInt(level)) {
                case 0:
                    row.push("detect evil");
                    break;
                case 1:
                    row.push("divine grace");
                    row.push("lay on hands");
                    break;
                case 2:
                    row.push("aura of courage");
                    break;
                case 5:
                    row.push("remove disease 1/week");
                    break;
                case 8:
                    row.push("remove disease 2/week");
                    break;
                case 11:
                    row.push("remove disease 3/week");
                    break;
                case 14:
                    row.push("remove disease 4/week");
                    break;
                case 17:
                    row.push("remove disease 5/week");
                    break;
            }
        break;
        case "f-unholygifts":
            switch(parseInt(level)) {
                case 0:
                    row.push("detect good");
                    row.push("poison use");
                    break;
                case 1:
                    row.push("dark blessing");
                    break;
                case 2:
                    row.push("aura of despair");
                    break;
            }        
        break;
        case "f-animalcompanion":
            switch(parseInt(value)) {
                case 1:
                    if (level == 3) {
                        row.push("animal companion");
                    }
                    break;
                case 2:
                    if (level == 0) {
                        row.push("animal companion");
                    }
                    break;
                case 3:
                    if (level == 0) {
                        row.push(`${$(".f-animalcompanion-box :selected").text().toLowerCase()} companion`);
                    }
                    break;
            }
        break;
        case "f-familiar":
            if (level == 0) {
                switch(parseInt(value)) {
                    case 1:
                        row.push("animal familiar");
                        break;
                    case 2:
                        row.push(`${$(".f-familiar-box :selected").text().toLowerCase()} familiar`)
                        break;
            }                     
        }
        break;
        case "f-wildshape":
            var shape1 = $(".f-wildshape-box-1 :selected").val();
            var shape2 = $(".f-wildshape-box-2 :selected").val();
            var shape3 = $(".f-wildshape-box-3 :selected").val();

            shaping = "";
            if (value == 1) {
                shaping = "small shapeshift";
            }
            else if (value == 3) {
                shaping = "wildshape";
            }

            switch(parseInt(level)) {
                case 4:
                    row.push(`${shaping}: ${shape1} (1/day)`);
                    break;
                case 5:
                    row.push(`${shaping}: ${shape1} (2/day)`);
                    break;
                case 6:
                    row.push(`${shaping}: ${shape1} (3/day)`);
                    break;
                case 7:
                    if (value == 3) {
                        row.push(`${shaping}: ${shape1} (Large)`);
                    }
                    break;
                case 9:
                    row.push(`${shaping}: ${shape1} (4/day)`);
                    break;
                case 10:
                    if (value == 3) {
                        row.push(`${shaping}: ${shape1} (Tiny)`);
                    }
                    break;
                case 11:
                    row.push(`${shaping}: ${shape2}`);
                    break;
                case 13:
                    row.push(`${shaping}: ${shape1} and ${shape2} (5/day)`);
                    break;
                case 14:
                    if (value == 3) {
                        row.push(`${shaping}: ${shape1} (Huge)`);
                    }
                    break;
                case 15:
                    row.push(`${shaping}: ${shape3} (1/day)`);
                    break;
                case 17:
                    row.push(`${shaping}: ${shape1} and ${shape2}  (6/day)`);
                    row.push(`${shaping}: ${shape3} (2/day)`);
                    break;
                case 19:
                    row.push(`${shaping}: ${shape3} 3/day`);
                    if (value == 3) {
                        row.push(`${shaping}: ${shape3} (Huge)`);
                    }
                    break;
            }
        break;
    }

    for (var i = 0; i < row.length; i++) {
        row[i] = capitalise(row[i]);
    }

    table[level] = row;
}

function buildFeatureDropdowns() {

    function getOptionsString(options) {
        var string = ``;
        for (let i = 0; i < options.length; i++) {
            
            var c = "";
            if(options[i].length > 1) {
                var clist = new Array();
                for (let j=1; j < options[i].length; j++) {
                    clist.push(`write-${options[i][j]}`);
                }
                c = `class="${clist.join(" ")}"`;
            }
            string += `<option ${c}>${options[i][0]}</option>`;
        }
        return string;
    }

    function getAllOptionsString(options, selectclass) {
        var nums = new Array();
        $(`#${selectclass}`).next().children(":first").find("option.box").each(function() {
            nums.push($(this).val());
        });
        var newoptions = new Array();
        for (var i=0; i < options.length; i++) {
            newoptions.push(options[i].concat(nums));
        }
        return getOptionsString(newoptions);
    }

    $(`.f-smite-box`).html(getOptionsString([
        ["Lawful", "2"],
        ["Chaotic", "2"],
        ["Evil", "2"],
        ["Good", "2"]
    ]));

    $(`.f-channel-box`).html(getOptionsString([
        ["Aberrations", "1"],
        ["Animals", "1"],
        ["Constructs", "1"],
        ["Dragons", "1"],
        ["Elementals", "1"],
        ["Fey", "1"],
        ["Giants", "1"],
        ["Humanoid", "1"],
        ["Magical Beasts", "1"],
        ["Monstrous Humanoids", "1"],
        ["Oozes", "1"],
        ["Outsiders", "1"],
        ["Plants", "1"],
        ["Undead", "1"],
        ["Vermins", "1"]
    ]));


    let wildshape = getOptionsString([
        ["Aberration"],
        ["Animal"],
        ["Construct"],
        ["Dragon"],
        ["Elemental"],
        ["Fey"],
        ["Giant"],
        ["Humanoid"],
        ["Magical Beast"],
        ["Monstrous Humanoid"],
        ["Ooze"],
        ["Outsider"],
        ["Plant"],
        ["Undead"],
        ["Vermin"]
    ]);

    $(`.f-wildshape-box-1`).html(wildshape);
    $(`.f-wildshape-box-2`).html(wildshape);
    $(`.f-wildshape-box-3`).html(wildshape);

    $(`.f-arcanecast-box`).html(getOptionsString([
        ["Sorcerer/Wizard", "1", "3", "10"],
        ["Bard", "1", "3"]
    ]));

    $(`.f-divinecast-box`).html(getOptionsString([
        ["Cleric", "1", "3", "8"],
        ["Druid", "1", "3", "8"],
        ["Paladin", "1"],
        ["Ranger", "1"]
    ]));

    let castingtype = [
        ["Vancian"],
        ["Spontaneous"]
    ];
    $(`.f-arcanecast-box-type`).html(getAllOptionsString(castingtype,"f-arcanecast"));
    $(`.f-divinecast-box-type`).html(getAllOptionsString(castingtype, "f-divinecast"));

    let castingstat = [
        ["Str"],
        ["Dex"],
        ["Con"],
        ["Int"],
        ["Wis"],
        ["Cha"]
    ];
    $(`.f-arcanecast-box-stat`).html(getAllOptionsString(castingstat,"f-arcanecast"));
    $(`.f-divinecast-box-stat`).html(getAllOptionsString(castingstat, "f-divinecast"));

    let animalcompanion = getOptionsString([
        ["Aberration"],
        ["Construct"],
        ["Dragon"],
        ["Elemental"],
        ["Magical Beast"],
        ["Ooze"],
        ["Outsider"],
        ["Plant"],
        ["Undead"],
        ["Vermin"]
    ]);

    $(`.f-animalcompanion-box`).html(animalcompanion);

    let familiar = getOptionsString([
        ["Aberration"],
        ["Construct"],
        ["Dragon"],
        ["Elemental"],
        ["Fey"],
        ["Ooze"],
        ["Outsider"],
        ["Plant"],
        ["Undead"],
        ["Vermin"]
    ]);

    $(`.f-familiar-box`).html(familiar);
}

function getFeatureDescriptors() {
    // gets the required detail for building descriptions in the template
    // f-name id
    // selectpicker selection
    // if exists, writebox-popup selections
    var descriptors = new Array();
    var popupselections;
    var selectedfeatures = getSelectedFeaturesById();

    for (let i = 0; i < selectedfeatures.length; i++) {
        popupselections = new Array();
        $(`#${selectedfeatures[i]}`).next().find(".writebox-popup select").each(function() {
            popupselections.push($(this).val());
        });

        descriptors.push(
            [
                selectedfeatures[i],
                $(`#${selectedfeatures[i]}`).next().find(".selectpicker :selected").val(),
                popupselections,
                $(`#${selectedfeatures[i]}`).next().find(".selectpicker :selected").text()
            ]
        );
    }

    return descriptors;
}

// --- GETTERS -----------------------------------------------------

function getKeyAbilityForSkill(abilityButton) {
    var keyability = "(None)";

    if(abilityButton.hasClass("badge-danger")) {
        keyability = "(Str)";
    }
    else if (abilityButton.hasClass("badge-dark")) {
        keyability = "(Dex)";
    }
    else if (abilityButton.hasClass("badge-success")) {
        keyability = "(Con)";
    }
    else if (abilityButton.hasClass("badge-primary")) {
        keyability = "(Int)";
    }
    else if (abilityButton.hasClass("badge-light")) {
        keyability = "(Wis)";
    }
    else if (abilityButton.hasClass("badge-warning")) {
        keyability = "(Cha)";
    }

    return keyability;
}

function getStats() {
    name = "" + $(".preview-name").val();
    if (name == "") {name = "Classname"};
    
    bab = $(".ability-bab select :selected").text();
    hd = $(".ability-hd select :selected").text();

    var selectedSaves = new Array();
    if ($(".ability-saves .save1").prop("checked")) {selectedSaves.push("Fortitude")};
    if ($(".ability-saves .save2").prop("checked")) {selectedSaves.push("Reflex")};
    if ($(".ability-saves .save3").prop("checked")) {selectedSaves.push("Will")};

    if (selectedSaves.length > 0) {
        saves = selectedSaves.join(", ");
    }
    else {
        saves = "none";
    }

    skillpoints = $(".ability-skillpoints select :selected").text();

    // skills = $(".ability-skills select :selected").text();
    var skillList = new Array();
    $(".ability-skills-picked :button").each(function() {
        skillList.push(`${$(this).text()} ${getKeyAbilityForSkill($(this))}`);
    });
    if (skillList.length > 0) {
        skills = skillList.join(", ");
    }
    else {
        skills = "none";
    }
    
    var weapons;
    switch($(".ability-weaponprof select :selected").val()) {
        case "1":
            weapons = "club, dagger, heavy crossbow, light crossbow, and quarterstaff";
            break;
        case "2":
            switch($(".ability-weaponprof select :selected").text()) {
                case "Druid":
                    weapons = "club, dagger, dart, quarterstaff, scimitar, sickle, shortspear, sling, and spear";
                    break;
                case "Monk":
                    weapons = "club, crossbow (light or heavy), dagger, handaxe, javelin, kama, nunchaku, quarterstaff, sai, shuriken, siangham, and sling";
                    break;
                default:
                    weapons = "all simple weapons";
            }
            break;
        case "3":
            weapons = "all simple weapons, plus the hand crossbow, rapier, sap, shortbow, and short sword";
            break;
        case "4":
            weapons = "all simple and martial weapons";
            break;
        default:
            weapons = "one simple weapon";
    }
    weaponprof = weapons;
    
    var armour;
    switch($(".ability-armourprof select :selected").val()) {
        case "1":
            armour = "light armour";
            break;
        case "2":
            armour = "light and medium armour";
            break;
        case "3":
            armour = "all armours";
            break;
        default:
            armour = "no armour";
    }
    armourprof = armour;

    var shield;
    switch($(".ability-shieldprof select :selected").val()) {
        case "1":
            shield = "all shields (except tower shields)";
            break;
        case "2":
            shield = "all shields including tower shields";
            break;
        default:
            shield = "no shields";
    }
    shieldprof = shield;

    if ($(".ability-equipment select :selected").val() == "0") {
        equiprestrict = "None";
    } else {
        equiprestrict = "" + $(".ability-equipment .restriction").val();
        if (equiprestrict == "") {equiprestrict = "Restricted"};
    }

    if ($(".ability-alignment select :selected").val() == "0") {
        alignrestrict = "None";
    } else {
        alignrestrict = "" + $(".ability-alignment .writebox-popup select :selected").val();
        if (alignrestrict == "") {alignrestrict = "Restricted"};
    }
}

// --- CLASS SHEET GENERATION --------------------------------------

function buildPage() {
    updateDialog();
    sessionStorage.setItem("vars", JSON.stringify({
        "name"          : name,
        "description"   : description,
        "bab"           : bab,
        "hd"            : hd,
        "saves"         : saves,
        "skillpoints"   : skillpoints,
        "skills"        : skills,
        "weaponprof"    : weaponprof,
        "armourprof"    : armourprof,
        "shieldprof"    : shieldprof,
        "equiprestrict" : equiprestrict,
        "alignrestrict" : alignrestrict,
        "special"       : constructTable(),
        "features"      : getFeatureDescriptors()
    }));
    window.open("template.html");
}

function constructTable() {
// loops through all 20 levels, and loops through every selected feature
// for each level. It starts building a table which will be used for
// showing a level progression table on the pdf
// returns an array object for use in a pdfmake table

    var classTable = new Array();
    var featureIds = new Array();
    featureIds = getSelectedFeaturesById();

    // loop through 20 levels
    for (i = 0; i < 20; i++) {
        classTable.push([])
        for (const feature of featureIds) {
            getFeatureLevel(i, feature, classTable);
        }
    }
    return Array.from(classTable.values());
}

function capitalise(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

// --- EVENT LISTENERS ---------------------------------------------

$("select.writebox").change(function(){
    if ($(this).find(":selected").prop("class") == "box") {
        $(this).parent().find(".writebox-popup").removeClass("d-none");
        if ($(this).parent().find(".writebox-popup").hasClass("writebox-select")) {
            applyBoxModifiers($(this).parent().prev().attr("id"));
        }   
    }
    else {
        $(this).parent().find(".writebox-popup").addClass("d-none");
    }
});

$("#pointstotal").change(function(){
    updatePoints();
});

$("#previewButton").on("change click", function() {
    updateDialog();
});

$("#abilities select").change(function() {
    updatePoints();
})

$("#features select").change(function() {
    updatePoints();
})

$(".ability-skills").change(function() {
    resetSkillsList();
})

$(".ability-saves").change(function() {
    switch(parseInt($(".ability-saves select :selected").val())) {
        case 0:
            $(".ability-saves .save1").removeClass("checked");
            $(".ability-saves .save1").prop('checked', false);
            $(".ability-saves .save2").removeClass("checked");
            $(".ability-saves .save2").prop('checked', false);
            $(".ability-saves .save3").removeClass("checked");
            $(".ability-saves .save3").prop('checked', false);
            break;
        case 4:
            $(".ability-saves .save1").addClass("checked");
            $(".ability-saves .save1").prop('checked', true);
            $(".ability-saves .save2").addClass("checked");
            $(".ability-saves .save2").prop('checked', true);
            $(".ability-saves .save3").addClass("checked");
            $(".ability-saves .save3").prop('checked', true);
            break;
        default:
    }
});
